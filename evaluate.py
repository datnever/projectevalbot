import torch
import torchvision
import torchvision.transforms as transforms
import os
import shutil
import stat

TEST_PATH = "val"
DEVICE = "cuda"


class Evaluator:
    def __init__(self, batch_size=64, num_workers=2):
        self.transform = transforms.Compose(
            [
             #Some inner logic
             ])

        self.testset = torchvision.datasets.ImageFolder(root=TEST_PATH, transform=self.transform)
        self.testloader = torch.utils.data.DataLoader(self.testset, batch_size=batch_size,
                                                      shuffle=False, num_workers=num_workers)
        self.classes = [x for x in range(1, 197)]
    
    def remove_readonly(self, func, path, _):
                    "Clear the readonly bit and reattempt the removal"
                    os.chmod(path, stat.S_IWRITE)
                    func(path)

    def evaluate(self, model_path, group, device=DEVICE):
        try:
            net = torch.jit.load(model_path, map_location='cpu').to(device)
            correct = 0
            total = 0
            with torch.no_grad():
                for data in self.testloader:
                    images, labels = data
                    if device == "mps" or "cuda":
                        images, labels = images.to(device), labels.to(device)
                    outputs = net(images)
                    _, predicted = torch.max(outputs.data, 1)
                    total += labels.size(0)
                    correct += (predicted == labels).sum().item()
                print(f'Accuracy - {100 * correct // total} %')
                acc = 100 * correct // total
                shutil.rmtree(f'{path}', onerror=self.remove_readonly)
                torch.cuda.empty_cache()
                return acc
        except:
            shutil.rmtree(f'{path}, onerror=self.remove_readonly)
            torch.cuda.empty_cache()

