import logging
import os
import sqlite3

from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import (
    Updater, 
    CommandHandler, 
    MessageHandler, 
    Filters, 
    CallbackContext, 
    ConversationHandler, 
    PicklePersistence
)
from dotenv import load_dotenv

from loader import load
from evaluate import Evaluator

load_dotenv()

TOKEN = os.getenv('BOT_TOKEN')
storage_path = 'models'


def start(update: Update, context: CallbackContext) -> None:
    user = update.effective_user
    update.message.reply_text(f'Привет, {user.username}!')
    try:
        sqlite_insert_with_param  = """INSERT INTO users
                            (username, user_id, user_name)
                            VALUES (?,?,?);"""
        data_tuple = (user.username, user.id, user.first_name)
        cursor.execute(sqlite_insert_with_param, data_tuple)
        conn.commit()
    except sqlite3.Error as error:
        logging.debug('Создание юзера не произошло. Пользователь существует.')
    keyboard = ReplyKeyboardMarkup([
                ['/help', '/group'],
                ['/load_model']
            ], resize_keyboard=True)
    update.message.reply_text('Для начала: \n 1. Выбери группу (команда "/group") \
                              \n 2. После выбора группы загрузи модель (команда "/load_model") \
                              \n При наличии вопросов (/help)', reply_markup=keyboard)


def help_command(update, context) -> None:
    update.message.reply_text('Чтобы загрузить работу: \n \
1. Введите номер своей команды \n \
2. Загрузите модель в формате .pt (подробная информация при вызове "/load_model") \n \
Ссылка на документ (https://vk.cc/cmKp6i) \n \
Ссылка на набор данных (https://vk.cc/cmKpbE) \n \n \
В случае ошибок пишите сюда @monoleet или @datnever (а лучше сразу всем).')


def pickTeam(update, context):
    cursor.execute("""SELECT team_id
                    FROM users
                    WHERE username=?
                        """,
                    (update.effective_user.username,))
    result = cursor.fetchone()
    if result[0] == None:
        update.message.reply_text("Введите номер команды")
        return 1
    else:
        update.message.reply_text("Вы уже выбрали команду. Если вы не выбирали - вызовите команду /help")
    

def saveAndShowTeam(update, context):
    team = update.message.text
    try:
        team = int(team)
    except:
        update.message.reply_text("Вызовите команду /group повторно и введите корректное значение.")
        return ConversationHandler.END
    
    if team>20 or team<0:
        update.message.reply_text("Вызовите команду /group повторно и введите корректное значение.")
        return ConversationHandler.END
    else:
        try:
            cursor.execute("""SELECT *
                        FROM users
                        WHERE team_id=?
                            """,
                        (team,))
            result = cursor.fetchone()
        except sqlite3.Error as error:
           logging.info("Ошибка при работе с SQLite", error)
        if  result:
            update.message.reply_text(
                f"Группа уже существет."
            )
        else:
            cursor.execute("UPDATE users set team_id=? where username=?", (team,  update.effective_user.username))
            update.message.reply_text(
                f"Ваша группа {team} записана.\n"
            )
            conn.commit()
        return ConversationHandler.END

def load_model(update, context):
    cursor.execute("""SELECT team_id
                FROM users
                WHERE username=?
                    """,
                (update.effective_user.username,))
    result = cursor.fetchone()
    if result[0]== None:
        update.message.reply_text("Команды не существует. Создайте команду.")
        return ConversationHandler.END
    else:
        update.message.reply_text("Загрузите модель в формате .pt в ваш открый репозиторий Github (1 репозиторий - 1 модель в нем). \n \
Затем вставьте ссылку на репозиторий (без последнего /). Для повторной проверки модели - удалите старую и загрузите новую. Если загрузка не началась - подождите, вы в очереди. \n \
Если загрузка не начинается больше 15 минут - что-то сломалось, вызовите /help. \n \
Отправьте ссылку следующим сообщением:")
        return 2
    
def SaveAndTestModel(update, context):
    update.message.reply_text("Начинаю процесс скачивания модели.")
    cursor.execute("""SELECT team_id
                FROM users
                WHERE username=?
                    """,
                (update.effective_user.username,))
    result = cursor.fetchone()
    if result[0]== None:
        update.message.reply_text("Команды не существует. Создайте команду.")
        return ConversationHandler.END
    try:
        model_path = load(result[0], update.message.text)
    except:
        logging.error('Не удалось получить путь модели.')
        update.message.reply_text("Не удалось получить путь модели.")
        return ConversationHandler.END

    if not os.path.exists(f'models/{result[0]}'):
        os.mkdir(f'models/{result[0]}')
    
    try:
        update.message.reply_text("🤖")
        update.message.reply_text("Модель загружена. Начинаю анализировать...")
    except:
        logging.error('Сообщение не отправлено.')

    try:
        evaluate = Evaluator(batch_size=64).evaluate
        val_result = evaluate(model_path=model_path, group = result[0])
    except:
        logging.error('Не удалось запустить проверку модели.')
        update.message.reply_text("Не удалось проанализировать. Попробуйте еще раз.")
        return ConversationHandler.END

    sqlite_insert_with_param  = """INSERT INTO eval
                            (model_eval, username)
                            VALUES (?,?);"""
    data_tuple = (val_result, update.effective_user.username)
    cursor.execute(sqlite_insert_with_param, data_tuple)
    conn.commit()
    update.message.reply_text("Модель проанализирована.")
    
    try:
        cursor.execute("""SELECT model_eval
                    FROM eval
                    WHERE username=?
                        """,
                    (update.effective_user.username,))
        last_result = cursor.fetchall()
        cursor.execute("""SELECT MAX(model_eval)
                    FROM eval
                    WHERE username=?
                        """,
                    (update.effective_user.username,))
        best_result = cursor.fetchone()
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    update.message.reply_text(f"Текущая проверка точности - {last_result[-1][0]}. Лучший результат - {best_result[0]}.")
    return ConversationHandler.END


def main():
    persistence = PicklePersistence(filename='bot_settings')
    updater = Updater(TOKEN, persistence=persistence, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help_command))

    handler = ConversationHandler(
        entry_points=[CommandHandler('group', pickTeam)],
        states={
            1: [MessageHandler(Filters.text & ~Filters.command, saveAndShowTeam)]
        },
        fallbacks=[],
    )
    handle2r = ConversationHandler(
        entry_points=[CommandHandler('load_model', load_model)],
        states={
            2: [MessageHandler(Filters.text & ~Filters.command, SaveAndTestModel)
                ]
        },
        fallbacks=[]
    )
    
    dispatcher.add_handler(handler)
    dispatcher.add_handler(handle2r)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        filename='bot.log',
        format='%(asctime)s, %(levelname)s, %(message)s'
    )
    try:
        conn = sqlite3.connect('db_leti.db', check_same_thread=False)
        cursor = conn.cursor()
        main()
    except KeyboardInterrupt:
        logging.info('Завершение работы.')