import os

from git import Git


def load(project_group: int, repo_link: str) -> str:
    """
    Loader for models
    :param project_group: Project group id
    :param repo_link: Link to git repo (main branch) with or w/o ".git" ending
    :return: path to model
    """

    splitter = '/'.join(repo_link.split('/')[len(repo_link.split('/')) - 2:])
    repo = splitter[:-4] \
        if ".git" in repo_link \
        else splitter

    if not str(project_group) in os.listdir("models"):
        os.mkdir(f"models/{str(project_group)}")
        print(repo)
        Git(f"models/{project_group}").clone(f"https://github.com/{repo}" + '.git')
    else:
        if not repo.split('/')[-1] in os.listdir(f"models/{project_group}/"):
            Git(f"models/{project_group}").clone(f"https://github.com/{repo}" + '.git')
        else:
            Git(f"models/{project_group}/{repo.split('/')[-1]}").pull()

    model_fetcher = os.listdir(f"models/{str(project_group)}/{repo.split('/')[-1]}")
    for model_path in model_fetcher:
        if ".pt" in model_path:
            path = f"models/{str(project_group)}/{repo.split('/')[-1]}/{model_path}"
            os.system(f"cd {'/'.join(path.split('/')[:-1])} && git lfs install && git lfs pull")
            return path



